#ifndef COMPILE_GLSL_H
#define COMPILE_GLSL_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path);

#endif
