// Include std libs
#include <stdio.h>
#include <stdlib.h>

// Include OpenGL libs
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

// Include glsl compiler
#include "compile_glsl.h"

// triangle
static const GLfloat g_triangle[] =
{
  -1.0f, -1.0f, 0.0f,
  1.0f, -1.0f, 0.0f,
  0.0f, 1.0f, 0.0f
};


int main()
{
  // init GLFW
  if(!glfwInit())
  {
    fprintf(stderr, "ERROR: Failed to init GLFW\n");
    return -1;
  }


  glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  // open a window
  GLFWwindow * window;
  window = glfwCreateWindow(800, 600, "OpenGl Experiment", NULL, NULL);
  if(!window)
  {
    fprintf(stderr, "ERROR: open a window\n");
    glfwTerminate();
    return -1;
  }

  // make context
  glfwMakeContextCurrent(window);

  // init glew
  if(glewInit() != GLEW_OK)
  {
    fprintf(stderr, "ERROR: Failed to init GLEW\n");
    glfwTerminate();
    return -1;
  }

  // vertex buffers
  GLuint vertexbuffer;
  glGenBuffers(1, &vertexbuffer);
  glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(g_triangle), g_triangle, GL_STATIC_DRAW);

  // vertex array
  GLuint VertexArrayID;
  glGenVertexArrays(1, &VertexArrayID);
  glBindVertexArray(VertexArrayID);

  // load glsl
  GLuint programID = LoadShaders(
    "src/glsl/SimpleVertexShader.vertexshader",
    "src/glsl/SimpleFragmentShader.fragmentshader");

  // loop until window is closed
  while (!glfwWindowShouldClose(window))
  {
    // clear the screen
    glClear(GL_COLOR_BUFFER_BIT);

    // use shaders
    glUseProgram(programID);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(
      0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0
    );

    glDrawArrays(GL_TRIANGLES, 0, 3);
    glDisableVertexAttribArray(0);

    // Swap front and back buffers
    glfwSwapBuffers(window);

    // Poll for and process events
    glfwPollEvents();
   }

  // delete buffer
  glDeleteBuffers(1, &vertexbuffer);
  glDeleteVertexArrays(1, &VertexArrayID);
	glDeleteProgram(programID);
  // close OpenGL
  glfwTerminate();
  return 0;
}
