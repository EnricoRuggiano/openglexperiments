cmake_minimum_required(VERSION 3.14)
project(OpenGLExperiment)

#link_directories(${CMAKE_SOURCE_DIR}/lib)

# Add source files
file(GLOB_RECURSE SOURCE_FILES
	${CMAKE_SOURCE_DIR}/src/*.cpp)

# Add header files
#file(GLOB_RECURSE HEADER_FILES
	#${CMAKE_SOURCE_DIR}/src/include/utils/*.h)

# Define the executable
add_executable(${PROJECT_NAME} ${SOURCE_FILES}) #${HEADER_FILES}

# find OpenGL packages
find_package(OpenGL REQUIRED)
find_package(glfw3 REQUIRED)
find_package(glm REQUIRED)
find_package(GLEW REQUIRED)


set(LIBS ${OPENGL_LIBRARY} glfw GLEW)

# add header files
include_directories(
"${CMAKE_SOURCE_DIR}/src/include"
"${CMAKE_SOURCE_DIR}/src/include/utils")

target_link_libraries(${PROJECT_NAME} ${LIBS})
